module "aws_config_rules" {
  source = "app.terraform.io/Remanence/config-rules/aws"

  tags = merge(
    { "service" = "config" },
    var.tags
  )
}
