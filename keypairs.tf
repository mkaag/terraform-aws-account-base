resource "aws_key_pair" "main" {
  key_name_prefix = "main-"
  public_key      = ""
}

output "keypair_main_name" {
  description = "The name of the main ssh keypair"
  value       = aws_key_pair.main.key_name
}
